package com.company;

public class DistanceBetweenPlanets {

    public static double calculate(SpaceObject source, SpaceObject destiny) {
        return Math.sqrt(Math.pow(source.getX() - destiny.getX(), 2)
                + Math.pow(source.getY() - destiny.getY(), 2)
                + Math.pow(source.getZ() - destiny.getZ(), 2));
    }

}
