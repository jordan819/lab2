package com.company;

public class Main {
    public static void main(String[] args) {

        SpaceObject earth = new SpaceObject(20.3, 100.3, 109);
        SpaceObject sun = new SpaceObject(0, 0, 0);

        double distance = DistanceBetweenPlanets.calculate(earth, sun);

        System.out.print(distance);

    }
}
